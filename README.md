# examen-navent

En este repositorio se encuentran 2 archivos solicitados por el examen y uno extra con las respuestas de la pregunta 2.


#### PedidosService.java

Es un servicio que maneja para pedidos las operaciones:
- Crear
- Modificar
- Buscar por id
- Borrar

El mismo utiliza a PedidosDAO para acceder y escribir en la base de datos.
Para reducir los accesos a la DB se utiliza un cache.

#### form.html

Es una pagina la cual sirve para guardar nuevos pedidos.
La misma chequea que:
- El nombre este presente y no sea mas largo de 100 caracteres
- El monto este presente y sea un numero entero
- El descuento sea un numero entero

Este formulario es enviado por medio de un request ajax.
A su respuesta, contesta si el mismo fue enviado o hubo un error

#### Pregunta2.txt

Un pequeño archivo txt que contiene los comentarios  que son la respuesta a la pregunta 2