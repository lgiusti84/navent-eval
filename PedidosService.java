package exam.luisgiusti.navent.services;

import exam.luisgiusti.navent.daos.BumexMemcached;
import exam.luisgiusti.navent.daos.PedidosDAO;
import exam.luisgiusti.navent.domain.Pedido;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Set;

@Service
public class PedidosService {
    // no se si BumexMemcached es solo para pedidos, por lo tanto
    private static final String PEDIDO_CACHE_PREFIX = Pedido.class.getName();
    private BumexMemcached cache = BumexMemcached.getInstance();

    // Buscar pedido por ID
    public Pedido getPedido(int id) {
        Pedido pedido = null;
        String cacheKey = pedidoIdToCacheKey(id);
        try {
            pedido = (Pedido) cache.get(cacheKey);
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }

        if(pedido == null) {
            pedido = PedidosDAO.select(id);
            if(pedido != null) {
                cache.set(cacheKey, pedido);
            }
        }

        return pedido;
    }

    // Crear pedido
    @Transactional
    public void savePedido(Pedido pedido) {
        PedidosDAO.insertOrUpdate(pedido);

        Integer pedidoId = pedido.getId();
        if(pedidoId != null) {
            cache.set(pedidoIdToCacheKey(pedidoId), pedido);
        }
    }

    // Modificar pedido
    @Transactional
    public void patchPedido(int id, Map<String, Object> partialPedido) {
        Pedido oldPedido = getPedido(id);

        if(oldPedido != null && !partialPedido.isEmpty()) {
            Set<String> keys = partialPedido.keySet();
            if(keys.contains("nombre")) {
                oldPedido.setNombre((String) partialPedido.get("nombre"));
            }
            if(keys.contains("monto")) {
                oldPedido.setMonto((Integer) partialPedido.get("monto"));
            }
            if(keys.contains("descuento")) {
                oldPedido.setDescuento((Integer) partialPedido.get("descuento"));
            }
            // Won't update id changes

            savePedido(oldPedido);
        }
    }

    // Borrar pedido
    public void deletePedido(int id) {
        cache.delete(pedidoIdToCacheKey(id));
        PedidosDAO.delete(PedidosDAO.select(id));
    }

    private String pedidoIdToCacheKey(int pedidoId) {
        return PEDIDO_CACHE_PREFIX + pedidoId;
    }

}
